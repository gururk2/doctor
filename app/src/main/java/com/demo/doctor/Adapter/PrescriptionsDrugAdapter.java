package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.PrescriptionsResponse;
import com.demo.doctor.R;

import java.util.List;

public class PrescriptionsDrugAdapter extends BaseQuickAdapter<PrescriptionsResponse, BaseViewHolder> {


    public PrescriptionsDrugAdapter(int layoutResId, @Nullable List<PrescriptionsResponse> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, PrescriptionsResponse item) {

        viewHolder.setText(R.id.prescription_name,item.getDrugName())
                .setText(R.id.prescription_dosage,item.getTimeOfTheDay())
                .setText(R.id.prescription_duration,item.getDuration())
                .setText(R.id.prescription_instruction,item.getToBeTaken())
                .setText(R.id.prescription_repeat,item.getRepeatSame());




    }
}
