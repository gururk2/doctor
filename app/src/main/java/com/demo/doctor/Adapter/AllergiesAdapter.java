package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.AllergyListResponse;
import com.demo.doctor.R;

import java.util.List;

public class AllergiesAdapter extends BaseQuickAdapter<AllergyListResponse, BaseViewHolder> {


    public AllergiesAdapter(int layoutResId, @Nullable List<AllergyListResponse> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, AllergyListResponse item) {

        viewHolder.setText(R.id.lab_allergy, item.getAllergyName())
                .setText(R.id.lab_allergy_name, item.getAllergyType())
                .setText(R.id.lab_test_date, item.getAppointmentOn());


    }

}