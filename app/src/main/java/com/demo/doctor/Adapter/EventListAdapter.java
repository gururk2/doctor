package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.DoctorEventResponse;
import com.demo.doctor.R;

import java.util.List;

public class EventListAdapter extends BaseQuickAdapter<DoctorEventResponse.Result, BaseViewHolder> {


    public EventListAdapter(int layoutResId, @Nullable List<DoctorEventResponse.Result> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, DoctorEventResponse.Result item) {
        viewHolder.setText(R.id.event_name, item.getEventName())
                .setText(R.id.event_date, item.getEventFromdatetime())
                .setText(R.id.event_desc, item.getEventDesc())
                .setText(R.id.event_location, item.getLocation());


    }
}
