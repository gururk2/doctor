package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.PatientResponse;
import com.demo.doctor.R;

import java.util.List;

public class PatientListAdapter extends BaseQuickAdapter<PatientResponse, BaseViewHolder> {


    public PatientListAdapter(int layoutResId, @Nullable List<PatientResponse> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, PatientResponse item) {
        viewHolder.setText(R.id.patient_name,item.getPatientName())
                .setText(R.id.patient_phone,item.getMobileNumber())
                .setText(R.id.patient_email,item.getEmail())
                .setText(R.id.patient_city,item.getAddress1());

        viewHolder.addOnClickListener(R.id.fab);
        viewHolder.addOnClickListener(R.id.mainLayout);


    }
}
