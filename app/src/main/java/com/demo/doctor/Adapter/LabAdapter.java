package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.LabListResponse;
import com.demo.doctor.R;

import java.util.List;

public class LabAdapter extends BaseQuickAdapter<LabListResponse, BaseViewHolder> {


    public LabAdapter(int layoutResId, @Nullable List<LabListResponse> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, LabListResponse item) {

        viewHolder.setText(R.id.lab_name, item.getLabName())
                .setText(R.id.lab_test_name, item.getTestName())
                .setText(R.id.lab_test_date, item.getAppointmentOn());


    }

}