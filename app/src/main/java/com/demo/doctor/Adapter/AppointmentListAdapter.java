package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.AppointmentResponse;
import com.demo.doctor.R;

import java.util.List;

public class AppointmentListAdapter extends BaseQuickAdapter<AppointmentResponse.Open, BaseViewHolder> {


    public AppointmentListAdapter(int layoutResId, @Nullable List<AppointmentResponse.Open> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, AppointmentResponse.Open item) {
        viewHolder.setText(R.id.patient_name, item.getPatientName())
                .setText(R.id.patient_phone, item.getMobileNumber())
                .setText(R.id.patient_email, item.getEmail())
                .setText(R.id.patient_city, "")
                .setText(R.id.patient_title, item.getTitle());


    }
}
