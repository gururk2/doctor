package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.VitalListResponse;
import com.demo.doctor.R;

import java.util.List;

public class VitalAdapter extends BaseQuickAdapter<VitalListResponse, BaseViewHolder> {


    public VitalAdapter(int layoutResId, @Nullable List<VitalListResponse> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, VitalListResponse item) {

        viewHolder.setText(R.id.vital_height, item.getHeight())
                .setText(R.id.vital_weight, item.getWeight())
                .setText(R.id.vital_bp, item.getBlood())
                .setText(R.id.vital_sugar, item.getSugar())
                .setText(R.id.vital_bmi, item.getBmi());


    }

}