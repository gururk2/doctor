package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.AllergyListResponse;

import java.util.List;

public class PrescriptionsAllergiesAdapter extends BaseQuickAdapter<AllergyListResponse, BaseViewHolder> {


    public PrescriptionsAllergiesAdapter(int layoutResId, @Nullable List<AllergyListResponse> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder viewHolder, AllergyListResponse item) {
    }
}
