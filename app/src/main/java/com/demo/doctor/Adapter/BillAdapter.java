package com.demo.doctor.Adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.demo.doctor.Model.BillResponse;
import com.demo.doctor.R;

import java.util.List;

public class BillAdapter extends BaseQuickAdapter<BillResponse, BaseViewHolder> {


public BillAdapter(int layoutResId, @Nullable List<BillResponse> data) {
        super(layoutResId, data);
        }

@Override
protected void convert(BaseViewHolder viewHolder, BillResponse item) {
        viewHolder.setText(R.id.patient_name,item.getPatientName())
                .setText(R.id.patient_payment_date,item.getPaymentDate())
                .setText(R.id.patient_payment_amount,item.getAmount())
                .setText(R.id.patient_payment_mode,item.getPaymentMode());





        }
        }
