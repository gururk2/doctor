package com.demo.doctor.Service;

import com.demo.doctor.Model.AllergyListResponse;
import com.demo.doctor.Model.AllergyTypeResponse;
import com.demo.doctor.Model.AppointmentResponse;
import com.demo.doctor.Model.BillResponse;
import com.demo.doctor.Model.CityResponse;
import com.demo.doctor.Model.DoctorEventResponse;
import com.demo.doctor.Model.DoctorPayBIllResponse;
import com.demo.doctor.Model.DoctorProfileResponse;
import com.demo.doctor.Model.DrugListResponse;
import com.demo.doctor.Model.DrugTypeResponse;
import com.demo.doctor.Model.LabListResponse;
import com.demo.doctor.Model.LoginResponse;
import com.demo.doctor.Model.PatientAddResponse;
import com.demo.doctor.Model.PatientResponse;
import com.demo.doctor.Model.PincodeResponse;
import com.demo.doctor.Model.PrescriptionsResponse;
import com.demo.doctor.Model.SpecialityResponse;
import com.demo.doctor.Model.StateResponse;
import com.demo.doctor.Model.SubSpecialityResponse;
import com.demo.doctor.Model.VitalListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> LOGIN_RESPONSE_CALL(@Field("email") String email, @Field("password") String password);

    @GET("patient_list.php")
    Call<List<PatientResponse>> PATIENT_RESPONSE_CALL();

    @FormUrlEncoded
    @POST("patient_registration.php")
    Call<PatientAddResponse> PATIENT_ADD_RESPONSE_CALL(
            @Field("fname") String fname,
            @Field("mname") String mname,
            @Field("patient_phone_number") String patient_phone_number,
            @Field("patient_mobile_number") String patient_mobile_number,
            @Field("address1") String address1,
            @Field("date_of_birth") String date_of_birth,
            @Field("social_security_identity") String social_security_identity,
            @Field("gender") String gender,
            @Field("martial_status") String martial_status,
            @Field("pincode") String pincode,
            @Field("state") String state,
            @Field("city") String city,
            @Field("patient_employer") String patient_employer,
            @Field("emergency_contact") String emergency_contact,
            @Field("relationship_to_patient") String relationship_to_patient,
            @Field("address2") String address2,
            @Field("recieveotp") String recieveotp,
            @Field("password") String password
    );


    @GET("appointment_list.php")
    Call<AppointmentResponse> APPOINTMENT_RESPONSE_CALL(@Query("id") String id);


    @GET("bill_list.php")
    Call<List<BillResponse>> BILL_RESPONSE_CALL(
            @Query("id") String id,
            @Query("fromDate") String fromDate,
            @Query("toDate") String toDate
    );

    @GET("get_doctor_details.php")
    Call<DoctorProfileResponse> DOCTOR_PROFILE_RESPONSE_CALL(@Query("id") String id);

    @POST("add_bill.php")
    Call<DoctorPayBIllResponse> DOCTOR_PAY_B_ILL_RESPONSE_CALL(
            @Query("id_doctor") String id_doctor,
            @Query("patient_id") String patient_id,
            @Query("date") String date,
            @Query("paymentmode") String paymentmode,
            @Query("amount") String amount
    );

    @GET("view_event.php")
    Call<DoctorEventResponse> DOCTOR_EVENT_RESPONSE_CALL(@Query("doctor_id") String id);


    @POST("add_event.php")
    Call<DoctorPayBIllResponse> DOCTOR_EVENT_ADD_RESPONSE_CALL(
            @Query("id_doctor") String id_doctor,
            @Query("event_name") String event_name,
            @Query("event_desc") String event_desc,
            @Query("event_fromdatetime") String event_fromdatetime,
            @Query("event_todatetime") String event_todatetime,
            @Query("location") String location
    );


    @GET("event_prescription.php")
    Call<List<PrescriptionsResponse>> PRESCRIPTION_LIST_RESPONSE_CALL(@Query("id") String id);

    @GET("event_lab.php")
    Call<List<LabListResponse>> LAB_LIST_RESPONSE_CALL(@Query("id") String id);

    @GET("event_vitals.php")
    Call<List<VitalListResponse>> VITAL_LIST_RESPONSE_CALL(@Query("id") String id);

    @GET("drug_list.php")
    Call<List<DrugListResponse>> DRUG_LIST_RESPONSE_CALL(@Query("id") String id);

    @GET("drug_type.php")
    Call<List<DrugTypeResponse>> DRUG_TYPE_RESPONSE_CALL();

    @GET("allergy_list.php")
    Call<List<AllergyListResponse>>ALLERGY_LIST_RESPONSE_CALL (@Query("id") String id);

    @GET("allergy_type.php")
    Call<List<AllergyTypeResponse>> ALLERGY_TYPE_RESPONSE_CALL();

    @POST("add_allergy.php")
    Call<DoctorPayBIllResponse> ALLERGY_ADD_RESPONSE_CALL(
            @Query("allergy_type") String allergy_type,
            @Query("allergy_name") String allergy_name,
            @Query("event_id") String event_id,
            @Query("patient_id") String patient_id

    );

    @POST("add_prescription.php")
    Call<DoctorPayBIllResponse> PRESCRIPTION_ADD_RESPONSE_CALL(
            @Query("drugDose") String drugDose,
            @Query("drugType") String drugType,
            @Query("duration") String duration,
            @Query("repeat") String repeat,
            @Query("time") String time,
            @Query("taken") String taken,
            @Query("event_id") String event_id,
            @Query("patient_id") String patient_id

    );

    @GET("state_list.php")
    Call<StateResponse> DOCTOR_SATE_RESPONSE_CALL();

    @GET("pincode_list.php")
    Call<List<PincodeResponse>> DOCTOR_PINCODE_RESPONSE_CALL(@Query("id") String id);

    @GET("city_list.php")
    Call<List<CityResponse>> DOCTOR_CITY_RESPONSE_CALL(@Query("id") String id);

    @GET("speciality_list.php")
    Call<List<SpecialityResponse>> DOCTOR_SPECIALITY_RESPONSE_CALL();

    @GET("sub_speciality_list.php")
    Call<List<SubSpecialityResponse>> DOCTOR_SUB_SPECIALITY_RESPONSE_CALL(@Query("id") String id);
}
