package com.demo.doctor.View.Doctor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.doctor.Adapter.AppointmentListAdapter;
import com.demo.doctor.Model.AppointmentResponse;
import com.demo.doctor.Model.PatientResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;
import com.demo.doctor.View.Doctor.PatientBillPayActivity;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentFragment extends Fragment {
    private String Tag = "AppointmentFragment";
    private RecyclerView mRecyclerView;
    private AppointmentListAdapter quickAdapter;
    private List<AppointmentResponse.Open> appointmentList;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appointments_list, container, false);
        // Inflate the layout for this fragment
        setToolbar(view);
        initView(view);
        callAppointmentList();

        return view;
    }

    void initView(View view) {
        mRecyclerView = view.findViewById(R.id.items_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void callAppointmentList() {
        ((BaseActivity) getActivity()).showLoading();
        Call<AppointmentResponse> appointmentListResponseCall = RetrofitClientInstance.getRetrofitInstance().APPOINTMENT_RESPONSE_CALL(DoctorConstants.PrefNames.DOCTOR_ID);
        appointmentListResponseCall.enqueue(new Callback<AppointmentResponse>() {
            @Override
            public void onResponse(Call<AppointmentResponse> call, Response<AppointmentResponse> response) {
                ((BaseActivity) getActivity()).hideLoading();
                if (response.isSuccessful()) {

                    appointmentList = response.body().getOpen();

                    if (appointmentList != null) {
                        InitData();
                    } else {
                        ((BaseActivity) getActivity()).showMessage("No Patients");
                    }


                } else {
                    ((BaseActivity) getActivity()).hideLoading();
                    ((BaseActivity) getActivity()).showMessage("No Patients");
                }
            }

            @Override
            public void onFailure(Call<AppointmentResponse> call, Throwable t) {
                ((BaseActivity) getActivity()).hideLoading();
                Log.e(Tag, t.getLocalizedMessage());
                ((BaseActivity) getActivity()).showMessage("Please try again");
            }
        });


    }

    private void InitData() {
        quickAdapter = new AppointmentListAdapter(R.layout.view_appointment_item, appointmentList);
        quickAdapter.openLoadAnimation();
        mRecyclerView.setAdapter(quickAdapter);
//        quickAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
//            @Override
//            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                if (view.getId() == R.id.fab) {
//                    callPay(appointmentList.get(position));
//                } else if (view.getId() == R.id.mainLayout) {
//                    startActivity(new Intent(getContext(), PatientDetailsActivity.class));
//                }
//
//            }
//        });
    }

    private void callPay(PatientResponse patientResponse) {
        Intent intent = new Intent(getActivity(), PatientBillPayActivity.class);
        intent.putExtra(DoctorConstants.Intent.PATIENT_DETAILS, patientResponse);
        startActivity(intent);
    }


    private void setToolbar(View view) {

        setHasOptionsMenu(true);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle("Appointment");
        toolbar.setTitle(null);
        ((BaseActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);


    }


}
