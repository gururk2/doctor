package com.demo.doctor.View.Doctor;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.demo.doctor.Model.CityResponse;
import com.demo.doctor.Model.PatientAddResponse;
import com.demo.doctor.Model.PatientResponse;
import com.demo.doctor.Model.PincodeResponse;
import com.demo.doctor.Model.StateResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.Utils.CommonUtils;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;
import com.tiper.MaterialSpinner;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

public class PatientAddEditActivity extends BaseActivity {
    EditText doctor_patient_fname, doctor_patient_mname, doctor_patient_lname, doctor_patient_email,
            doctor_patient_password, doctor_patient_mobile, doctor_patient_alter_mobile, doctor_patient_address,
            doctor_patient_practice_employer, doctor_patient_emergency_contact, doctor_patient_dob;
    MaterialSpinner patient_employment_status, doctor_patient_otp, doctor_patient_gender, doctor_patient_martial_status;
    AutoCompleteTextView doctor_patient_city, doctor_patient_state, doctor_patient_pincode;
    private static final String[] EMPLOYMENT_STATUS = {"Full Time", "Part Time", "Business", "Unemployed"};
    private static final String[] GENDER = {"Male", "Female", "Other"};
    private static final String[] MARTIAL_STATUS = {"SINGLE", "MARRIED", "Other"};
    private static final String[] OTP = {"Mobile Number", "Cell Phone Number", "Alternate Number"};
    AwesomeValidation mAwesomeValidation;
    PatientResponse patientResponse;
    String PATIENT_INPUT_TYPE = "";

    String cityId = "",
            stateId = "",
            pincodeId = "",
            patient_otp = "",
            patient_gender = "",
            patient_emp_status = "",
            patient_martial_status = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details__add_edit);
        setToolbar();
        initView();
        initData();
        callEdit();
        onClick();
    }


    private void initView() {

        doctor_patient_fname = findViewById(R.id.doctor_patient_fname);
        doctor_patient_mname = findViewById(R.id.doctor_patient_mname);
        doctor_patient_lname = findViewById(R.id.doctor_patient_lname);
        doctor_patient_email = findViewById(R.id.doctor_patient_email);
        doctor_patient_password = findViewById(R.id.doctor_patient_password);
        doctor_patient_mobile = findViewById(R.id.doctor_patient_mobile);
        doctor_patient_alter_mobile = findViewById(R.id.doctor_patient_alter_mobile);
        doctor_patient_address = findViewById(R.id.doctor_patient_address);
        doctor_patient_practice_employer = findViewById(R.id.doctor_patient_practice_employer);
        doctor_patient_pincode = findViewById(R.id.doctor_patient_pincode);
        doctor_patient_emergency_contact = findViewById(R.id.doctor_patient_emergency_contact);
        doctor_patient_dob = findViewById(R.id.doctor_patient_dob);

        doctor_patient_gender = findViewById(R.id.doctor_patient_gender);
        doctor_patient_martial_status = findViewById(R.id.doctor_patient_martial_status);
        doctor_patient_otp = findViewById(R.id.doctor_patient_otp);
        patient_employment_status = findViewById(R.id.patient_employment_status);

        doctor_patient_city = findViewById(R.id.doctor_patient_city);
        doctor_patient_state = findViewById(R.id.doctor_patient_state);

        doctor_patient_dob.setFocusable(false);
        doctor_patient_dob.setClickable(true);

        loadState();

        findViewById(R.id.patient_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getIntent().getExtras().get(DoctorConstants.Intent.PATIENT_INPUT_TYPE).equals("edit")) {
                    callUpload();
                } else {
                    if (mAwesomeValidation.validate())
                        callUpload();
                }

            }
        });

    }

    private void loadState() {
        showLoading();
        Call<StateResponse> stateResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_SATE_RESPONSE_CALL();
        stateResponseCall.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                hideLoading();
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (!response.body().getError()) {
                        List<String> StateList = new ArrayList<>();

                        for (StateResponse.Datum datum : response.body().getData()) {
                            StateList.add(datum.getState());
                        }

                        doctor_patient_state.setAdapter(new ArrayAdapter<String>(PatientAddEditActivity.this, R.layout.spinner_view, StateList));
                        doctor_patient_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                InputMethodManager imm = (InputMethodManager) PatientAddEditActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                                for (StateResponse.Datum datum : response.body().getData()) {
                                    if (datum.getState().equals(doctor_patient_state.getText().toString())) {
                                        stateId = String.valueOf(datum.getId());
                                    }
                                }


                                loadCity();

                            }
                        });

                    } else {
                        showMessage("Cant'tload state , please try agin");
                    }
                    hideLoading();
                } else {
                    showMessage("Please try again");
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });
    }

    void loadCity() {
        showLoading();
        Call<List<CityResponse>> stateResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_CITY_RESPONSE_CALL(stateId);
        stateResponseCall.enqueue(new Callback<List<CityResponse>>() {
            @Override
            public void onResponse(Call<List<CityResponse>> call, Response<List<CityResponse>> response) {
                hideLoading();
                if (response.code() == 200) {
                    List<String> cityList = new ArrayList<>();

                    for (CityResponse datum : response.body()) {
                        cityList.add(datum.getCity());
                    }
                    doctor_patient_city.setVisibility(View.VISIBLE);
                    doctor_patient_city.setAdapter(new ArrayAdapter<String>(PatientAddEditActivity.this, R.layout.spinner_view, cityList));
                    doctor_patient_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            InputMethodManager imm = (InputMethodManager) PatientAddEditActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                            for (CityResponse datum : response.body()) {
                                if (datum.getCity().equals(doctor_patient_city.getText().toString())) {
                                    cityId = String.valueOf(datum.getId());
                                }
                            }

                            callPincode();

                        }
                    });

                } else {
                    showMessage("Cant'tload city , please try agin");
                }
                hideLoading();

            }

            @Override
            public void onFailure(Call<List<CityResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });
    }

    private void callPincode() {

        showLoading();
        Call<List<PincodeResponse>> stateResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_PINCODE_RESPONSE_CALL(cityId);
        stateResponseCall.enqueue(new Callback<List<PincodeResponse>>() {
            @Override
            public void onResponse(Call<List<PincodeResponse>> call, Response<List<PincodeResponse>> response) {
                hideLoading();
                if (response.code() == 200) {
                    List<String> pincodeList = new ArrayList<>();

                    for (PincodeResponse datum : response.body()) {
                        pincodeList.add(datum.getPincode());
                    }
                    doctor_patient_pincode.setVisibility(View.VISIBLE);
                    doctor_patient_pincode.setAdapter(new ArrayAdapter<String>(PatientAddEditActivity.this, R.layout.spinner_view, pincodeList));
                    doctor_patient_pincode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            InputMethodManager imm = (InputMethodManager) PatientAddEditActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);


                            for (PincodeResponse datum : response.body()) {
                                if (datum.getPincode().equals(doctor_patient_pincode.getText().toString())) {
                                    pincodeId = String.valueOf(datum.getId());
                                }
                            }


                        }
                    });

                } else {
                    showMessage("Cant'tload pincode , please try agin");
                }
                hideLoading();

            }

            @Override
            public void onFailure(Call<List<PincodeResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });
    }


    private void callEdit() {
        if (getIntent().getExtras().get(DoctorConstants.Intent.PATIENT_INPUT_TYPE).equals("edit")) {
            PatientResponse patientResponse = (PatientResponse) getIntent().getExtras().get(DoctorConstants.Intent.PATIENT_DETAILS);


            doctor_patient_fname.setText(patientResponse.getFirstName());
            doctor_patient_mname.setText(patientResponse.getMiddleName());
            doctor_patient_lname.setText(patientResponse.getLastName());
            doctor_patient_email.setText(patientResponse.getEmail());
            doctor_patient_password.setText(patientResponse.getPassword());
            doctor_patient_mobile.setText(patientResponse.getMobileNumber());
            doctor_patient_alter_mobile.setText(patientResponse.getPatientPhoneNumber());
            doctor_patient_address.setText(patientResponse.getAddress1());
            doctor_patient_practice_employer.setText(patientResponse.getPatientEmployer());
            doctor_patient_emergency_contact.setText(patientResponse.getEmergencyContact());
            doctor_patient_dob.setText(patientResponse.getDateOfBirth());
            doctor_patient_city.setVisibility(View.VISIBLE);
            doctor_patient_state.setVisibility(View.VISIBLE);
            doctor_patient_pincode.setVisibility(View.VISIBLE);
            stateId = patientResponse.getState();
            cityId = patientResponse.getCity();
            pincodeId = patientResponse.getPincode();
            doctor_patient_gender.setSelection(Arrays.asList(GENDER).indexOf(patientResponse.getGender()));
            doctor_patient_martial_status.setSelection(Arrays.asList(MARTIAL_STATUS).indexOf(patientResponse.getMartialStatus()));
            doctor_patient_otp.setSelection(Arrays.asList(OTP).indexOf(patientResponse.getOtp()));
            patient_employment_status.setSelection(Arrays.asList(EMPLOYMENT_STATUS).indexOf(patientResponse.getEmploymentStatus()));

            doctor_patient_dob.setText(patientResponse.getDateOfBirth());
            hideLoading();

        }
    }

    private void onClick() {
        doctor_patient_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(PatientAddEditActivity.this);
                showDateTime(PatientAddEditActivity.this, doctor_patient_dob);
            }
        });
    }

    private void initData() {

        // new CountryHelper(PatientAddEditActivity.this, doctor_patient_city, doctor_patient_state);
        doctor_patient_otp.setAdapter(new ArrayAdapter<String>(this, R.layout.spinner_view, OTP));
        doctor_patient_gender.setAdapter(new ArrayAdapter<String>(this, R.layout.spinner_view, GENDER));
        patient_employment_status.setAdapter(new ArrayAdapter<String>(this, R.layout.spinner_view, EMPLOYMENT_STATUS));
        doctor_patient_martial_status.setAdapter(new ArrayAdapter<String>(this, R.layout.spinner_view, MARTIAL_STATUS));
        mAwesomeValidation = new AwesomeValidation(BASIC);
        validation();
        patient_employment_status.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view, int i, long l) {
                patient_emp_status = materialSpinner.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

            }
        });
        doctor_patient_otp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view, int i, long l) {
                patient_otp = materialSpinner.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

            }
        });
        doctor_patient_gender.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view, int i, long l) {
                patient_gender = materialSpinner.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

            }
        });
        doctor_patient_martial_status.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view, int i, long l) {
                patient_martial_status = materialSpinner.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

            }
        });

    }


    void validation() {
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_fname, "[a-zA-Z\\s]+", R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_mname, "[a-zA-Z\\s]+", R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_lname, "[a-zA-Z\\s]+", R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_email, android.util.Patterns.EMAIL_ADDRESS, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_password, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_mobile, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_alter_mobile, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_address, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_pincode, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_emergency_contact, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_practice_employer, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_city, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_state, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_gender, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.patient_employment_status, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_otp, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.doctor_patient_martial_status, RegexTemplate.NOT_EMPTY, R.string.err);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void callUpload() {
        showLoading();
        Call<PatientAddResponse> addResponseCall = RetrofitClientInstance.getRetrofitInstance().PATIENT_ADD_RESPONSE_CALL(
                doctor_patient_fname.getText().toString(),
                doctor_patient_mname.getText().toString(),
                doctor_patient_alter_mobile.getText().toString(),
                doctor_patient_mobile.getText().toString(),
                doctor_patient_address.getText().toString(),
                doctor_patient_dob.getText().toString(),
                "312312",
                patient_gender,
                patient_martial_status,
                pincodeId,
                stateId,
                cityId,
                doctor_patient_practice_employer.getText().toString(),
                doctor_patient_emergency_contact.getText().toString(),
                patient_emp_status,
                "",
                patient_otp,
                doctor_patient_password.getText().toString()
        );

        addResponseCall.enqueue(new Callback<PatientAddResponse>() {
            @Override
            public void onResponse(Call<PatientAddResponse> call, Response<PatientAddResponse> response) {
                hideLoading();
                if (response.code() == 200) {
                    if (response.body().getStatus().equals(200)) {
                        showMessage(response.body().getMessage());
                        CommonUtils.putSharedPrefsString(getApplicationContext(), DoctorConstants.PrefNames.DOCTOR_PATIENT_REFRESH, "1");
                        finish();

                    } else {
                        showMessage("Cant't add patient , please try agin");
                    }
                    hideLoading();
                } else {
                    showMessage("Please try again");
                }
            }

            @Override
            public void onFailure(Call<PatientAddResponse> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });
    }


    private void setToolbar() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable drawable = toolbar.getNavigationIcon();
        drawable.setColorFilter(ContextCompat.getColor(this, R.color.md_white_1000), PorterDuff.Mode.SRC_ATOP);

    }




}


