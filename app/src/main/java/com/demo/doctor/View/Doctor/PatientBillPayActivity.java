package com.demo.doctor.View.Doctor;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.demo.doctor.Model.DoctorPayBIllResponse;
import com.demo.doctor.Model.PatientResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.Utils.CommonUtils;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;
import com.tiper.MaterialSpinner;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

public class PatientBillPayActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private MaterialSpinner patient_payment_mode;
    private String[] payment_mode = {"Cash", "Credit Card", "Debit Card"};
    ArrayAdapter<String> paymentArrayAdapter;
    private PatientResponse patientResponse;
    private EditText patient_name, patient_payment_date, patient_amount;
    private AwesomeValidation mAwesomeValidation;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_bill_payment);
        setToolbar();
        initView();
        initData();
        initValidate();


    }


    private void initView() {
        patient_name = findViewById(R.id.patient_name);
        patient_name = findViewById(R.id.patient_name);
        patient_payment_date = findViewById(R.id.patient_payment_date);
        patient_amount = findViewById(R.id.patient_amount);
        patient_payment_mode = findViewById(R.id.patient_payment_mode);

    }


    private void initData() {
        if (getIntent() != null)
            patientResponse = (PatientResponse) Objects.requireNonNull(getIntent().getExtras()).getSerializable(DoctorConstants.Intent.PATIENT_DETAILS);
        patient_payment_date.setFocusable(false);
        patient_payment_date.setClickable(true);
        patient_name.setText(patientResponse.getPatientName());
        paymentArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_view, payment_mode);
        patient_payment_mode.setAdapter(paymentArrayAdapter);
        patient_payment_date.setOnClickListener(this);
        findViewById(R.id.patient_pay).setOnClickListener(this);
    }


    private void setToolbar() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Bill Payment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }

    void initValidate() {
        mAwesomeValidation = new AwesomeValidation(BASIC);

        mAwesomeValidation.addValidation(this, R.id.patient_payment_date, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.patient_amount, RegexTemplate.NOT_EMPTY, R.string.err);
    }

    void callBillPay() {
        if (mAwesomeValidation.validate()) {
            showLoading();
            Call<DoctorPayBIllResponse> doctorPayBIllResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_PAY_B_ILL_RESPONSE_CALL(
                    CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ID),
                    patientResponse.getId(),
                    patient_payment_date.getText().toString(),
                    patient_payment_mode.getSelectedItem().toString(),
                    patient_amount.getText().toString()
            );
            doctorPayBIllResponseCall.enqueue(new Callback<DoctorPayBIllResponse>() {
                @Override
                public void onResponse(Call<DoctorPayBIllResponse> call, Response<DoctorPayBIllResponse> response) {
                    hideLoading();
                    if (response.code() == 200) {


                        assert response.body() != null;
                        showMessage(response.body().getMessage());
                        finish();


                    } else {
                        showMessage("Please try again");
                    }
                }

                @Override
                public void onFailure(Call<DoctorPayBIllResponse> call, Throwable t) {
                    hideLoading();
                    Timber.e(t.getLocalizedMessage());
                    showMessage("Please try again");
                }
            });
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.patient_payment_date: {
                showDateTime(PatientBillPayActivity.this, patient_payment_date);
                break;

            }
            case R.id.patient_pay: {
                callBillPay();
                break;
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

}
