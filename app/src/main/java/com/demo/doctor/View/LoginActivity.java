package com.demo.doctor.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.demo.doctor.Model.LoginResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.Utils.CommonUtils;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.Doctor.DoctorDashboardActivity;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;


public class LoginActivity extends BaseActivity {
    Button facebook;
    EditText reg_username, reg_password;
    AwesomeValidation mAwesomeValidation;
    String tag = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mAwesomeValidation = new AwesomeValidation(BASIC);
        reg_username = findViewById(R.id.reg_username);
        reg_password = findViewById(R.id.reg_password);
        mAwesomeValidation.addValidation(this, R.id.reg_username, android.util.Patterns.EMAIL_ADDRESS, R.string.err_email);
        mAwesomeValidation.addValidation(this, R.id.reg_password, RegexTemplate.NOT_EMPTY, R.string.err_pass);

        facebook = findViewById(R.id.login);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mAwesomeValidation.validate()) {
                    hideSoftKeyboard(LoginActivity.this);
                    showLoading();
                    callLogin();
                }
            }
        });
    }


    void callLogin() {
//yogeshdims@gmail.com
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Call<LoginResponse> loginResponseCall = RetrofitClientInstance.getRetrofitInstance().LOGIN_RESPONSE_CALL(Objects.requireNonNull(reg_username.getText()).toString().trim(), Objects.requireNonNull(reg_password.getText()).toString().trim());
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code() == 200) {
                    if (response.body().getStatus().equals(200)) {
                        showMessage("Welcome " + response.body().getData().getDoctorName());
                        //saveData(response.body().getData());
                        finishAffinity();
                        startActivity(new Intent(LoginActivity.this, DoctorDashboardActivity.class));
                    } else {
                        showMessage("Please enter valid credentials");
                    }
                    hideLoading();
                } else {
                    showMessage("Please try again");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                hideLoading();
                Timber.tag(tag).e(t.getLocalizedMessage());
                showMessage("Please try again");
            }
        });
    }

    private void saveData(LoginResponse.Data body) {
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_NAME, body.getDoctorName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_F_NAME, body.getFirstName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_M_NAME, body.getMiddleName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_L_NAME, body.getLastName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_EMAIL, body.getEmail());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PASSWORD, body.getPassword());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_GENDER, body.getGender());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_SPECIALTY, body.getSpecialty());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_TELEPHONE, body.getTelephone());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_MOBILE, body.getMobile());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_DOB, body.getDateOfBirth());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CELL, body.getCellNumber());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ALTER_NUMBER, body.getAlterNumber());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ADDRESS, body.getAddress());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PINCODE, body.getPincode());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_STATE, body.getState());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CITY, body.getCity());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PRACTICE_CONTACT, body.getPracticeContact());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CLINC_ADDRESS, body.getClinicAddress());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_EXPERENCE, body.getExperiance());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_QUALIFICATION, body.getQualification());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_INSTITUTE, body.getInstitute());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_FELLOWSHIP, body.getFellowship());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_IMA_NO, body.getImaNo());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_LICENCE, body.getLicence());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_RECIVE_OTP, body.getReceiveOtp());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PHOTO, body.getPhoto());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_OTP, body.getReceiveOtp());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_STATUS, body.getStatus());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_TIME_SLOAT, body.getTimeslot());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PATIENT_REFRESH, "0");


    }
}
