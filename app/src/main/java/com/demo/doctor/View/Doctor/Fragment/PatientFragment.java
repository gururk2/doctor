package com.demo.doctor.View.Doctor.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.demo.doctor.Adapter.PatientListAdapter;
import com.demo.doctor.Model.PatientResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.Utils.CommonUtils;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;
import com.demo.doctor.View.Doctor.PatientAddEditActivity;
import com.demo.doctor.View.Doctor.PatientBillPayActivity;
import com.demo.doctor.View.Doctor.PatientDetailsActivity;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class PatientFragment extends Fragment {
    String tag = "PatientFragment";
    private RecyclerView mRecyclerView;
    private PatientListAdapter quickAdapter;
    private List<PatientResponse> patientList;
    private Toolbar toolbar;
    private SwipeRefreshLayout swipeContainer;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient_list, container, false);
        // Inflate the layout for this fragment
        setToolbar(view);
        mRecyclerView = view.findViewById(R.id.items_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        CommonUtils.putSharedPrefsString(getContext(), DoctorConstants.PrefNames.DOCTOR_PATIENT_REFRESH, "0");
        callPatientList();

        return view;
    }

    private void callPatientList() {
        ((BaseActivity) getActivity()).showLoading();
        Call<List<PatientResponse>> patientListResponseCall = RetrofitClientInstance.getRetrofitInstance().PATIENT_RESPONSE_CALL();
        patientListResponseCall.enqueue(new Callback<List<PatientResponse>>() {
            @Override
            public void onResponse(Call<List<PatientResponse>> call, Response<List<PatientResponse>> response) {
                ((BaseActivity) getActivity()).hideLoading();
                if (response.isSuccessful()) {
                    patientList = response.body();


                    InitData();

                } else {
                    ((BaseActivity) getActivity()).showMessage("No Patients");
                }
            }

            @Override
            public void onFailure(Call<List<PatientResponse>> call, Throwable t) {
                ((BaseActivity) getActivity()).hideLoading();
                Log.e(tag, t.getLocalizedMessage());
                ((BaseActivity) getActivity()).showMessage("Please try again");
            }
        });


    }

    private void InitData() {
        quickAdapter = new PatientListAdapter(R.layout.view_patient_item, patientList);
        quickAdapter.openLoadAnimation();
        mRecyclerView.setAdapter(quickAdapter);
        quickAdapter.notifyDataSetChanged();
        if (swipeContainer.isRefreshing()) {
            swipeContainer.setRefreshing(false);
        }
        quickAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (view.getId() == R.id.fab) {
                    callPay(patientList.get(position));
                } else if (view.getId() == R.id.mainLayout) {
                    callPatientDetails(patientList.get(position));
                }

            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callPatientList();
            }
        });

    }

    private void callPay(PatientResponse patientResponse) {
        Intent intent = new Intent(getActivity(), PatientBillPayActivity.class);
        intent.putExtra(DoctorConstants.Intent.PATIENT_DETAILS, patientResponse);
        startActivity(intent);
    }

    private void callPatientDetails(PatientResponse patientResponse) {
        Intent intent = new Intent(getActivity(), PatientDetailsActivity.class);
        intent.putExtra(DoctorConstants.Intent.PATIENT_INPUT_TYPE, "");
        intent.putExtra(DoctorConstants.Intent.PATIENT_DETAILS, patientResponse);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add:
                Intent intent = new Intent(getActivity(), PatientAddEditActivity.class);
                intent.putExtra(DoctorConstants.Intent.PATIENT_INPUT_TYPE, "");
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbar(View view) {

        setHasOptionsMenu(true);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle("Patient list");
        toolbar.setTitle(null);
        ((BaseActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);


    }

    private void loadFragment(Fragment fragment) {

        FragmentTransaction transaction = Objects.requireNonNull(getFragmentManager()).beginTransaction();
        transaction.replace(R.id.frame_container, fragment); // give your fragment container id in first parameter
        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
        transaction.commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Context context = getContext();
                if (context != null) {
                    if (CommonUtils.getSharedPrefsString(getContext(), DoctorConstants.PrefNames.DOCTOR_PATIENT_REFRESH).equals("1")) {
                        callPatientList();
                        CommonUtils.putSharedPrefsString(getContext(), DoctorConstants.PrefNames.DOCTOR_PATIENT_REFRESH, "0");
                    }
                }
                Timber.d("onAttache");
            }
        }, TimeUnit.SECONDS.toMillis(0));

    }
}
