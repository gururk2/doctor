package com.demo.doctor.View.Doctor;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.demo.doctor.Adapter.BillAdapter;
import com.demo.doctor.Model.BillResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.View.BaseActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.basgeekball.awesomevalidation.ValidationStyle.TEXT_INPUT_LAYOUT;

public class DoctorBillActivity extends BaseActivity {

    private TextView payment_total;
    private TextInputEditText from_date, to_date;
    private FloatingActionButton done;
    private BillAdapter billAdapter;
    private RecyclerView mRecyclerView;
    private List<BillResponse> billResponses;
    private Toolbar toolbar;
    private AwesomeValidation mAwesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_list);
        initView();
        onClick();


    }

    private void initView() {

        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        done = findViewById(R.id.done);
        payment_total = findViewById(R.id.payment_total);
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAwesomeValidation = new AwesomeValidation(TEXT_INPUT_LAYOUT);
        mAwesomeValidation.addValidation(this, R.id.from_date_textInput, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.to_date_textInput, RegexTemplate.NOT_EMPTY, R.string.err);
        from_date.setFocusable(false);
        from_date.setClickable(true);
        to_date.setFocusable(false);
        to_date.setClickable(true);

    }

    private void initData() {
        billAdapter = new BillAdapter(R.layout.view_bill_item, billResponses);
        billAdapter.openLoadAnimation();
        mRecyclerView.setAdapter(billAdapter);


    }

    private void callBillSummary() {
        showLoading();
        Call<List<BillResponse>> billResponseCall = RetrofitClientInstance.getRetrofitInstance().BILL_RESPONSE_CALL("1", from_date.getText().toString().trim(), to_date.getText().toString().trim());
        billResponseCall.enqueue(new Callback<List<BillResponse>>() {
            @Override
            public void onResponse(Call<List<BillResponse>> call, Response<List<BillResponse>> response) {
                hideLoading();
                if (response.isSuccessful()) {
                    if (response.body().isEmpty()) {
                        showMessage("NO BIll Found");
                    } else {

                    }
                    billResponses = response.body();

                    payment_total.setText(getSum(billResponses));


                    initData();
                }
            }


            @Override
            public void onFailure(Call<List<BillResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");
            }
        });


    }

    String getSum(List<BillResponse> billResponse_) {
        int sum = 0;
        for (BillResponse billResponse : billResponse_) {
            sum += Integer.parseInt(billResponse.getAmount());

        }
        return String.valueOf(sum);
    }

    private void onClick() {
        from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTime(DoctorBillActivity.this, from_date);
            }
        });
        to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTime(DoctorBillActivity.this, to_date);
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAwesomeValidation.validate())
                    callBillSummary();
            }
        });
    }
}
