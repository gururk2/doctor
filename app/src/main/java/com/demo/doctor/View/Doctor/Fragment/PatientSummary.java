package com.demo.doctor.View.Doctor.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.doctor.Adapter.PrescriptionsAdapter;
import com.demo.doctor.R;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class PatientSummary extends BaseActivity {

    String type;
    PrescriptionsAdapter prescriptionsAdapter;
    List<String> strings;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_patient_list);
        initView();
        initData();
        setToolbar();
    }


    private void initData() {
        strings = new ArrayList<>();
        strings.add("test");
        strings.add("test");
        strings.add("test");


        prescriptionsAdapter = new PrescriptionsAdapter(getIntent().getExtras().getInt("layout"),
                strings);
        recyclerView.setAdapter(prescriptionsAdapter);

//        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (getIntent().getExtras().getInt("Addlayout") == 0) {
//                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//                    intent.addCategory(Intent.CATEGORY_OPENABLE);
//                    startActivityForResult(intent, DoctorConstants.IntentRequestCodes.REQUEST_CAPTURE_IMAGE);
//
//                } else {
//
//                    AlertDialog alertDialog = new AlertDialog.Builder(PatientSummary.this).create();
//                    View alertLayout = getLayoutInflater().inflate(getIntent().getExtras().getInt("Addlayout"), null);
//                    alertDialog.setView(alertLayout);
//
//                    alertDialog.setTitle("Add");
//
//                    alertDialog.setIcon(R.mipmap.ic_launcher);
//
//                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancle", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//
//                    alertDialog.show();
//                }
//
//            }
//        });
    }

    private void initView() {
        recyclerView = findViewById(R.id.items_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                if (getIntent().getExtras().getInt("Addlayout") == 0) {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(intent, DoctorConstants.IntentRequestCodes.REQUEST_CAPTURE_IMAGE);

                } else {

                    AlertDialog alertDialog = new AlertDialog.Builder(PatientSummary.this).create();
                    View alertLayout = getLayoutInflater().inflate(getIntent().getExtras().getInt("Addlayout"), null);
                    alertDialog.setView(alertLayout);

                    alertDialog.setTitle("Add");

                    alertDialog.setIcon(R.mipmap.ic_launcher);

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancle", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertDialog.show();
                }
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setToolbar() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }
}
