package com.demo.doctor.View.Doctor.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demo.doctor.R;
import com.demo.doctor.View.Doctor.PatientAllergiesSummary;
import com.demo.doctor.View.Doctor.PatientDrugSummary;
import com.demo.doctor.View.Doctor.PatientLabSummary;
import com.demo.doctor.View.Doctor.PatientVitalSummary;


public class DoctorPatientPrescriptionsFragment extends Fragment {

   private LinearLayout patient_prescription,patient_lab,patient_allergies,patient_vitals,patient_file;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_patient_summary, container, false);
        initView(view);
        initData();
        return view;
    }

    private void initData() {

        patient_prescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              Intent intent = new Intent(getActivity(), PatientDrugSummary.class);
              intent.putExtra("layout",R.layout.view_prescriptions_item);
              intent.putExtra("Addlayout",R.layout.view_add_prescription);
              startActivity(intent);
            }
        });
        patient_lab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PatientLabSummary.class);
                intent.putExtra("layout",R.layout.view_lab_item);
                intent.putExtra("Addlayout",R.layout.view_add_lab);
                startActivity(intent);
            }
        });
        patient_allergies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PatientAllergiesSummary.class);
                intent.putExtra("layout",R.layout.view_allergies);

                intent.putExtra("Addlayout",R.layout.view_add_allergies);
                startActivity(intent);
            }
        });
        patient_vitals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PatientVitalSummary.class);
                intent.putExtra("layout",R.layout.view_vitals);
                intent.putExtra("Addlayout",R.layout.view_add_vitals);
                intent.putExtra("Addlayout",0);
                startActivity(intent);
            }
        });
        patient_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(),PatientSummary.class);
                intent.putExtra("layout",R.layout.view_files);
                startActivity(intent);
            }
        });

    }

    private void initView(View view) {

        patient_prescription = view.findViewById(R.id.patient_prescription);
        patient_lab = view.findViewById(R.id.patient_lab);
        patient_allergies = view.findViewById(R.id.patient_allergies);
        patient_vitals = view.findViewById(R.id.patient_vitals);
        patient_file = view.findViewById(R.id.patient_file);



    }


}
