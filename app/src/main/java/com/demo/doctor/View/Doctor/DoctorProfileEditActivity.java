package com.demo.doctor.View.Doctor;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.demo.doctor.Model.SpecialityResponse;
import com.demo.doctor.Model.SubSpecialityResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.Utils.CommonUtils;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;
import com.tiper.MaterialSpinner;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class DoctorProfileEditActivity extends BaseActivity {
    TextView doctor_fname, doctor_mname, doctor_lname, doctor_email, doctor_password,
            doctor_mobile, doctor_alter_mobile, doctor_address, doctor_practice_contact, doctor_hospital_address, doctor_experience,
            doctor_qualification, doctor_university, doctor_fellowship_number, doctor_ASSoc_number, doctor_licence, doctor_time_slot, doctor_pincode;
    MaterialSpinner doctor_gender, doctor_speciality, doctor_sub_speciality;
    AutoCompleteTextView doctor_state, doctor_city;
    String speciality = "", sub_speciality = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_doctor_profile_edit);
        setToolbar();
        initView();
        initData();

    }

    private void initView() {
        doctor_fname = findViewById(R.id.doctor_fname);
        doctor_mname = findViewById(R.id.doctor_mname);
        doctor_lname = findViewById(R.id.doctor_lname);
        doctor_email = findViewById(R.id.doctor_email);
        doctor_password = findViewById(R.id.doctor_password);
        doctor_gender = findViewById(R.id.doctor_gender);
        doctor_speciality = findViewById(R.id.doctor_speciality);
        doctor_sub_speciality = findViewById(R.id.doctor_sub_speciality);
        doctor_mobile = findViewById(R.id.doctor_mobile);
        doctor_alter_mobile = findViewById(R.id.doctor_alter_mobile);
        doctor_address = findViewById(R.id.doctor_address);
        doctor_pincode = findViewById(R.id.doctor_pincode);

        doctor_state = findViewById(R.id.doctor_state);
        doctor_city = findViewById(R.id.doctor_city);
        doctor_practice_contact = findViewById(R.id.doctor_practice_contact);
        doctor_hospital_address = findViewById(R.id.doctor_hospital_address);
        doctor_experience = findViewById(R.id.doctor_experience);
        doctor_qualification = findViewById(R.id.doctor_qualification);

        doctor_university = findViewById(R.id.doctor_university);
        doctor_fellowship_number = findViewById(R.id.doctor_fellowship_number);
        doctor_ASSoc_number = findViewById(R.id.doctor_ASSoc_number);
        doctor_licence = findViewById(R.id.doctor_licence);
        doctor_time_slot = findViewById(R.id.doctor_time_slot);
        findViewById(R.id.profile_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initData() {

        doctor_fname.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_F_NAME)));
        doctor_mname.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_M_NAME)));
        doctor_lname.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_L_NAME)));
        doctor_email.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_EMAIL)));
        doctor_password.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PASSWORD)));
        // doctor_gender.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_GENDER  )));
        doctor_fname.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_SPECIALTY)));
        doctor_alter_mobile.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_TELEPHONE)));
        doctor_mobile.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_MOBILE)));
        doctor_fname.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_DOB)));
        doctor_fname.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CELL)));
        doctor_fname.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ALTER_NUMBER)));
        doctor_address.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ADDRESS)));
        doctor_pincode.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PINCODE)));
        doctor_state.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_STATE)));
        doctor_city.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CITY)));
        doctor_practice_contact.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PRACTICE_CONTACT)));
        doctor_hospital_address.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CLINC_ADDRESS)));
        doctor_experience.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_EXPERENCE)));
        doctor_qualification.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_QUALIFICATION)));
        doctor_university.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_INSTITUTE)));
        doctor_fellowship_number.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_FELLOWSHIP)));
        doctor_ASSoc_number.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_IMA_NO)));
        doctor_licence.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_LICENCE)));
        doctor_time_slot.setText((CommonUtils.getSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_RECIVE_OTP)));

        initSpeciality();

    }

    private void initSpeciality() {
        showLoading();
        Call<List<SpecialityResponse>> SpecialityResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_SPECIALITY_RESPONSE_CALL();
        SpecialityResponseCall.enqueue(new Callback<List<SpecialityResponse>>() {
            @Override
            public void onResponse(Call<List<SpecialityResponse>> call, Response<List<SpecialityResponse>> response) {
                hideLoading();
                if (response.code() == 200) {
                    List<String> cityList = new ArrayList<>();

                    ArrayAdapter<SpecialityResponse> adapter =
                            new ArrayAdapter<SpecialityResponse>(getApplicationContext(),  R.layout.spinner_view
                                    , response.body());
                    adapter.setDropDownViewResource(R.layout.spinner_view);

                    doctor_speciality.setAdapter(adapter);

                    doctor_speciality.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view, int i, long l) {
                            InputMethodManager imm = (InputMethodManager) DoctorProfileEditActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
//

                            speciality = ((SpecialityResponse)materialSpinner.getSelectedItem()).getId();

                            initSubSpeciality();

                        }

                        @Override
                        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
                        }
                    });




                } else {
                    showMessage("Cant'tload city , please try agin");
                }
                hideLoading();

            }

            @Override
            public void onFailure(Call<List<SpecialityResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });
    }

    private void initSubSpeciality() {
        showLoading();
        Call<List<SubSpecialityResponse>> SubSpecialityResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_SUB_SPECIALITY_RESPONSE_CALL(speciality);
        SubSpecialityResponseCall.enqueue(new Callback<List<SubSpecialityResponse>>() {
            @Override
            public void onResponse(Call<List<SubSpecialityResponse>> call, Response<List<SubSpecialityResponse>> response) {
                hideLoading();
                if (response.code() == 200) {
                    List<String> cityList = new ArrayList<>();

                    ArrayAdapter<SubSpecialityResponse> adapter =
                            new ArrayAdapter<SubSpecialityResponse>(DoctorProfileEditActivity.this,  R.layout.spinner_view
                                    , response.body());
                    adapter.setDropDownViewResource(R.layout.spinner_view);

                    doctor_sub_speciality.setAdapter(adapter);

                    doctor_sub_speciality.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view, int i, long l) {
                            InputMethodManager imm = (InputMethodManager) DoctorProfileEditActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
//

                            sub_speciality = ((SubSpecialityResponse)materialSpinner.getSelectedItem()).getId();



                        }

                        @Override
                        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

                        }
                    });




                } else {
                    showMessage("Cant'tload city , please try agin");
                }
                hideLoading();

            }

            @Override
            public void onFailure(Call<List<SubSpecialityResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setToolbar() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable drawable = toolbar.getNavigationIcon();
        drawable.setColorFilter(ContextCompat.getColor(this, R.color.md_white_1000), PorterDuff.Mode.SRC_ATOP);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
