package com.demo.doctor.View.Doctor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.demo.doctor.Adapter.EventListAdapter;
import com.demo.doctor.Model.DoctorEventResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.View.BaseActivity;
import com.demo.doctor.View.Doctor.EventAddActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class DoctorEventFragment extends Fragment {
    RecyclerView recyclerView;
    FloatingActionButton add;
    Toolbar toolbar;
    private EventListAdapter eventListAdapter;
    private SwipeRefreshLayout swipeContainer;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_event, container, false);
        initView(view);
        setToolbar(view);
        initDate();


        return view;
    }

    private void initDate() {
        ((BaseActivity) getActivity()).showLoading();
        Call<DoctorEventResponse> eventResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_EVENT_RESPONSE_CALL("1");
        eventResponseCall.enqueue(new Callback<DoctorEventResponse>() {
            @Override
            public void onResponse(Call<DoctorEventResponse> call, Response<DoctorEventResponse> response) {
                ((BaseActivity) getActivity()).hideLoading();
                if (response.code() == 200) {

                    if (!response.body().getResult().isEmpty()) {
                        eventListAdapter = new EventListAdapter(R.layout.view_event_item, response.body().getResult());

                        recyclerView.setAdapter(eventListAdapter);
                        eventListAdapter.notifyDataSetChanged();
                        if(swipeContainer.isRefreshing()){
                            swipeContainer.setRefreshing(false);
                        }
                    } else {
                        ((BaseActivity) getActivity()).showMessage("No Event");
                    }


                } else {
                    ((BaseActivity) getActivity()).showMessage("Please try again");
                }
            }


            @Override
            public void onFailure(Call<DoctorEventResponse> call, Throwable t) {
                ((BaseActivity) getActivity()).hideLoading();
                Timber.e(t.getLocalizedMessage());
                ((BaseActivity) getActivity()).showMessage("Please try again");
            }
        });




    }

    private void initView(View view) {

        recyclerView = view.findViewById(R.id.recyclerView);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initDate();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add:
                startActivity(new Intent(getActivity(), EventAddActivity.class));

        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbar(View view) {

        setHasOptionsMenu(true);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle("Patient list");
        toolbar.setTitle(null);
        ((BaseActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);


    }


}
