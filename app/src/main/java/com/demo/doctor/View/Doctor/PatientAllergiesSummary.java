package com.demo.doctor.View.Doctor;

import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.doctor.Adapter.PrescriptionsAllergiesAdapter;
import com.demo.doctor.Model.AllergyListResponse;
import com.demo.doctor.Model.AllergyTypeResponse;
import com.demo.doctor.Model.DoctorPayBIllResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.View.BaseActivity;
import com.tiper.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class PatientAllergiesSummary extends BaseActivity {

    String type;
    PrescriptionsAllergiesAdapter prescriptionsAdapter;
    List<String> strings;
    RecyclerView recyclerView;
    List<AllergyTypeResponse> allergyTypeResponses;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_patient_list);
        initView();
        initData();
        loadType();
        setToolbar();
    }


    private void initData() {
        showLoading();
        Call<List<AllergyListResponse>> allergyListResponseCall = RetrofitClientInstance.getRetrofitInstance().ALLERGY_LIST_RESPONSE_CALL("1");

        allergyListResponseCall.enqueue(new Callback<List<AllergyListResponse>>() {
            @Override
            public void onResponse(Call<List<AllergyListResponse>> call, Response<List<AllergyListResponse>> response) {
                hideLoading();
                if (response.code() == 200) {
                    if (response.body().isEmpty()) {
                        showMessage("Empty");

                    } else {
                        prescriptionsAdapter = new PrescriptionsAllergiesAdapter(R.layout.view_allergies,
                                response.body());
                        recyclerView.setAdapter(prescriptionsAdapter);
                        prescriptionsAdapter.notifyDataSetChanged();
                    }

                } else {
                    showMessage("Please try again");
                }

            }

            @Override
            public void onFailure(Call<List<AllergyListResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });


    }


    void loadType() {

        Call<List<AllergyTypeResponse>> allergyTypeResponseCall = RetrofitClientInstance.getRetrofitInstance().ALLERGY_TYPE_RESPONSE_CALL();
        allergyTypeResponseCall.enqueue(new Callback<List<AllergyTypeResponse>>() {
            @Override
            public void onResponse(Call<List<AllergyTypeResponse>> call, Response<List<AllergyTypeResponse>> response) {
                hideLoading();
                if (response.code() == 200) {
                    if (response.body().isEmpty()) {
                        showMessage("Empty");

                    } else {
                        allergyTypeResponses = new ArrayList<>();
                        allergyTypeResponses = response.body();
                    }

                } else {
                    showMessage("Please try again");
                }

            }

            @Override
            public void onFailure(Call<List<AllergyTypeResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });

    }

    void addAllergy(String allergy_type, String allergy_name) {
        showLoading();
        Call<DoctorPayBIllResponse> allergyTypeResponseCall = RetrofitClientInstance.getRetrofitInstance().ALLERGY_ADD_RESPONSE_CALL(
                allergy_type,
                allergy_name,
                "11111",
                "1"
        );
        allergyTypeResponseCall.enqueue(new Callback<DoctorPayBIllResponse>() {
            @Override
            public void onResponse(Call<DoctorPayBIllResponse> call, Response<DoctorPayBIllResponse> response) {
                hideLoading();
                if (response.code() == 200) {
                    showMessage(response.body().getMessage());
                    initData();
                } else {
                    showMessage("Please try again");
                }

            }

            @Override
            public void onFailure(Call<DoctorPayBIllResponse> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });
    }

    private void initView() {
        recyclerView = findViewById(R.id.items_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:


                AlertDialog alertDialog = new AlertDialog.Builder(PatientAllergiesSummary.this).create();
                View alertLayout = getLayoutInflater().inflate(R.layout.view_add_allergies, null);

                TextView allergy_add_name = alertLayout.findViewById(R.id.allergy_add_name);
                MaterialSpinner allergy_add_type = alertLayout.findViewById(R.id.allergy_add_type);


                alertDialog.setView(alertLayout);

                ArrayAdapter<AllergyTypeResponse> adapter =
                        new ArrayAdapter<AllergyTypeResponse>(getApplicationContext(), R.layout.spinner_view
                                , allergyTypeResponses);
                adapter.setDropDownViewResource(R.layout.spinner_view);
                allergy_add_type.setAdapter(adapter);

                alertDialog.setTitle("Add");

                alertDialog.setIcon(R.mipmap.ic_launcher);

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        addAllergy(((AllergyTypeResponse) allergy_add_type.getSelectedItem()).getId(), allergy_add_name.getText().toString());


                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog.show();

                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setToolbar() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.md_white_1000));
        toolbar.setTitle("Allergy Summary");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable drawable = toolbar.getNavigationIcon();
        drawable.setColorFilter(ContextCompat.getColor(this, R.color.md_white_1000), PorterDuff.Mode.SRC_ATOP);


    }
}
