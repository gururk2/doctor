package com.demo.doctor.View.Doctor;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.demo.doctor.Model.DoctorPayBIllResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.View.BaseActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

public class EventAddActivity extends BaseActivity {

    EditText event_name, event_desc, event_from, event_to, event_location;
    FloatingActionButton addEventImage;
    Button event_save;
    AwesomeValidation mAwesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_add_event);

        initView();
        setToolbar();
        onClick();
        initValidation();

    }

    private void initView() {

        event_name = findViewById(R.id.event_name);
        event_desc = findViewById(R.id.event_desc);
        event_from = findViewById(R.id.event_from);
        event_to = findViewById(R.id.event_to);
        event_location = findViewById(R.id.event_location);
        addEventImage = findViewById(R.id.addEventImage);
        event_save = findViewById(R.id.event_save);

        event_from.setFocusable(false);
        event_from.setClickable(true);

        event_to.setFocusable(false);
        event_to.setClickable(true);





    }

    private void initValidation() {

        mAwesomeValidation = new AwesomeValidation(BASIC);
        mAwesomeValidation.addValidation(this, R.id.event_name,  RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.event_desc,  RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.event_from, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.event_to, RegexTemplate.NOT_EMPTY, R.string.err);
        mAwesomeValidation.addValidation(this, R.id.event_location, RegexTemplate.NOT_EMPTY, R.string.err);


    }

    private void addEvent() {
        showLoading();
        Call<DoctorPayBIllResponse> addEventCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_EVENT_ADD_RESPONSE_CALL(
                "1",
                event_name.getText().toString(),
                event_desc.getText().toString(),
                event_from.getText().toString(),
                event_to.getText().toString(),
                event_location.getText().toString()
        );

        addEventCall.enqueue(new Callback<DoctorPayBIllResponse>() {
            @Override
            public void onResponse(Call<DoctorPayBIllResponse> call, Response<DoctorPayBIllResponse> response) {
                hideLoading();
                if (response.code() == 200) {
                    showMessage(response.body().getMessage());
                    finish();

                } else {
                    showMessage("Try again");
                }


            }

            @Override
            public void onFailure(Call<DoctorPayBIllResponse> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");
            }
        });

    }


    private void onClick() {
        event_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAwesomeValidation.validate()) {
                    addEvent();
                }
            }
        });

        event_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTime(EventAddActivity.this, event_from);
            }
        });

        event_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTime(EventAddActivity.this, event_to);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    private void setToolbar() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable drawable = toolbar.getNavigationIcon();
        drawable.setColorFilter(ContextCompat.getColor(this, R.color.md_white_1000), PorterDuff.Mode.SRC_ATOP);

    }
}
