package com.demo.doctor.View.Doctor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.demo.doctor.R;
import com.demo.doctor.Utils.CommonUtils;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;
import com.demo.doctor.View.Doctor.DoctorProfileEditActivity;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DoctorProfileFragment extends Fragment {
    private ProfileTabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    TextView doctor_profile_name, doctor_profile_qualification, doctor_profile_experiance;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_profile, container, false);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        doctor_profile_name = view.findViewById(R.id.doctor_profile_name);
        doctor_profile_qualification = view.findViewById(R.id.doctor_profile_qualification);
        doctor_profile_experiance = view.findViewById(R.id.doctor_profile_experiance);
        doctor_profile_name.setText(CommonUtils.getSharedPrefsString(getContext(), DoctorConstants.PrefNames.DOCTOR_NAME));
        doctor_profile_qualification.setText(CommonUtils.getSharedPrefsString(getContext(), DoctorConstants.PrefNames.DOCTOR_QUALIFICATION));
        doctor_profile_experiance.setText("Exp: " + CommonUtils.getSharedPrefsString(getContext(), DoctorConstants.PrefNames.DOCTOR_EXPERENCE) + "years");
        adapter = new ProfileTabAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new DoctorDetailsFragment(), "INFO");
        adapter.addFragment(new DoctorReviewFragment(), "REVIEWS");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        setToolbar(view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.edit:
                startActivity(new Intent(getActivity(), DoctorProfileEditActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbar(View view) {

        setHasOptionsMenu(true);
        Toolbar toolbar = view.findViewById(R.id.toolbar);

        toolbar.setTitle(null);

        ((BaseActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);

    }








    private class ProfileTabAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ProfileTabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }
}




