package com.demo.doctor.View.Doctor;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.demo.doctor.Model.DoctorProfileResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.Utils.CommonUtils;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.BaseActivity;
import com.demo.doctor.View.Doctor.Fragment.AppointmentFragment;
import com.demo.doctor.View.Doctor.Fragment.DoctorEventFragment;
import com.demo.doctor.View.Doctor.Fragment.DoctorProfileFragment;
import com.demo.doctor.View.Doctor.Fragment.HomeFragment;
import com.demo.doctor.View.Doctor.Fragment.PatientFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashMap;
import java.util.Stack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DoctorDashboardActivity extends BaseActivity {
    private HashMap<String, Stack<Fragment>> mStacks;
    public static final String TAB_HOME = "tab_home";
    public static final String TAB_PATIENT = "tab_patient";
    public static final String TAB_APPOINTMENT = "tab_appointment";
    public static final String TAB_SEARCH = "tab_search";
    public static final String TAB_PROFILE = "tab_profile";
    BottomNavigationView navView;
    Toolbar toolbarTop;
    private Fragment fragment;
    private int saveState;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_appointment:
                    fragment = new AppointmentFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_patient:
                    fragment = new PatientFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_search:
                    fragment = new DoctorEventFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_profile:
                    fragment = new DoctorProfileFragment();
                    loadFragment(fragment);
                    return true;

            }
            return false;
        }
    };

    private void callProfile() {
        showLoading();
        Call<DoctorProfileResponse> doctorProfileResponseCall = RetrofitClientInstance.getRetrofitInstance().DOCTOR_PROFILE_RESPONSE_CALL("1");
        doctorProfileResponseCall.enqueue(new Callback<DoctorProfileResponse>() {
            @Override
            public void onResponse(Call<DoctorProfileResponse> call, Response<DoctorProfileResponse> response) {
                hideLoading();
                if (response.code() == 200) {

                    if (!response.body().getResult().isEmpty()) {
                        setData(response.body().getResult().get(0));
                    } else {
                        showMessage("Unable to load profile data");
                    }


                } else {
                    showMessage("Please try again");
                }
            }

            @Override
            public void onFailure(Call<DoctorProfileResponse> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");
            }
        });


    }

    private void setData(DoctorProfileResponse.Result body) {
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ID, body.getId());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_NAME, body.getDoctorName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_F_NAME, body.getFirstName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_M_NAME, body.getMiddleName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_L_NAME, body.getLastName());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_EMAIL, body.getEmail());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PASSWORD, body.getPassword());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_GENDER, body.getGender());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_SPECIALTY, body.getSpecialty());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_TELEPHONE, body.getTelephone());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_MOBILE, body.getMobile());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_DOB, body.getDateOfBirth());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CELL, body.getCellNumber());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ALTER_NUMBER, body.getAlterNumber());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_ADDRESS, body.getAddress());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PINCODE, body.getPincode());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_STATE, body.getState());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CITY, body.getCity());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PRACTICE_CONTACT, body.getPracticeContact());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_CLINC_ADDRESS, body.getClinicAddress());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_EXPERENCE, body.getExperiance());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_QUALIFICATION, body.getQualification());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_INSTITUTE, body.getInstitute());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_FELLOWSHIP, body.getFellowship());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_IMA_NO, body.getImaNo());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_LICENCE, body.getLicence());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_RECIVE_OTP, body.getReceiveOtp());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_PHOTO, body.getPhoto());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_OTP, body.getReceiveOtp());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_STATUS, body.getStatus());
        CommonUtils.putSharedPrefsString(this, DoctorConstants.PrefNames.DOCTOR_TIME_SLOAT, body.getTimeslot());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        callProfile();
        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mStacks = new HashMap<String, Stack<Fragment>>();
        mStacks.put(TAB_HOME, new Stack<Fragment>());
        mStacks.put(TAB_APPOINTMENT, new Stack<Fragment>());
        mStacks.put(TAB_PATIENT, new Stack<Fragment>());
        mStacks.put(TAB_SEARCH, new Stack<Fragment>());
        mStacks.put(TAB_PROFILE, new Stack<Fragment>());

        if (savedInstanceState != null) {
            navView.setSelectedItemId(saveState);
        } else {
            navView.setSelectedItemId(R.id.navigation_home);
        }

    }


    private void loadFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (navView.getSelectedItemId() == R.id.navigation_home) {
           finishAffinity();
        } else {
            navView.setSelectedItemId(R.id.navigation_home);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        navView.setSelectedItemId(saveState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        saveState = navView.getSelectedItemId();
    }


}
