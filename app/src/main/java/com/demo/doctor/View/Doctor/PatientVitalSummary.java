package com.demo.doctor.View.Doctor;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.demo.doctor.Adapter.VitalAdapter;
import com.demo.doctor.Model.VitalListResponse;
import com.demo.doctor.R;
import com.demo.doctor.Service.RetrofitClientInstance;
import com.demo.doctor.View.BaseActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class PatientVitalSummary extends BaseActivity {
    VitalAdapter vitalAdapter;
    private  RecyclerView recyclerView;
    private SwipeRefreshLayout swipeContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_patient_list);
        initView();
        initData();
        setToolbar();
    }


    private void initData() {
        showLoading();
        Call<List<VitalListResponse>> allergyListResponseCall = RetrofitClientInstance.getRetrofitInstance().VITAL_LIST_RESPONSE_CALL("57");

        allergyListResponseCall.enqueue(new Callback<List<VitalListResponse>>() {
            @Override
            public void onResponse(Call<List<VitalListResponse>> call, Response<List<VitalListResponse>> response) {
                hideLoading();
                if (response.code() == 200) {
                    if (response.body().isEmpty()) {
                        showMessage("Empty");

                    } else {
                        vitalAdapter = new VitalAdapter(R.layout.view_vitals, response.body());
                        recyclerView.setAdapter(vitalAdapter);
                        vitalAdapter.notifyDataSetChanged();
                        if (swipeContainer.isRefreshing()) {
                            swipeContainer.setRefreshing(false);
                        }
                    }

                } else {
                    showMessage("Please try again");
                }

            }

            @Override
            public void onFailure(Call<List<VitalListResponse>> call, Throwable t) {
                hideLoading();
                Timber.e(t.getLocalizedMessage());
                showMessage("Please try again");

            }
        });


    }


    private void initView() {
        recyclerView = findViewById(R.id.items_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:


//                AlertDialog alertDialog = new AlertDialog.Builder(PatientDrugSummary.this).create();
//                View alertLayout = getLayoutInflater().inflate(R.layout.view_add_prescription, null);
//
//                AutoCompleteTextView drug_add_name = alertLayout.findViewById(R.id.drug_add_name);
//                drug_add_name.setVisibility(View.GONE);
//                EditText drug_add_duration = alertLayout.findViewById(R.id.drug_add_duration);
//                EditText drug_add_repeat = alertLayout.findViewById(R.id.drug_add_repeat);
//                EditText drug_add_time = alertLayout.findViewById(R.id.drug_add_time);
//                EditText drug_add_take = alertLayout.findViewById(R.id.drug_add_take);
//                MaterialSpinner drug_add_type = alertLayout.findViewById(R.id.drug_add_type);
//                AwesomeValidation mAwesomeValidation = new AwesomeValidation(BASIC);
//                drug_add_repeat.setFocusable(false);
//                drug_add_repeat.setClickable(true);
//
//                drug_add_time.setFocusable(false);
//                drug_add_time.setClickable(true);
//
//                alertDialog.setView(alertLayout);
//                mAwesomeValidation.addValidation(this, R.id.drug_add_name, RegexTemplate.NOT_EMPTY, R.string.err);
//                mAwesomeValidation.addValidation(this, R.id.drug_add_duration, RegexTemplate.NOT_EMPTY, R.string.err);
//                mAwesomeValidation.addValidation(this, R.id.drug_add_repeat, RegexTemplate.NOT_EMPTY, R.string.err);
//                mAwesomeValidation.addValidation(this, R.id.drug_add_time, RegexTemplate.NOT_EMPTY, R.string.err);
//                mAwesomeValidation.addValidation(this, R.id.drug_add_take, RegexTemplate.NOT_EMPTY, R.string.err);
//                mAwesomeValidation.addValidation(this, R.id.drug_add_type, RegexTemplate.NOT_EMPTY, R.string.err);
//
//                drug_add_type.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view, int i, long l) {
//                        drug_add_name.setVisibility(View.VISIBLE);
//
//                        Timber.e(((DrugTypeResponse) materialSpinner.getSelectedItem()).getId());
//                        loadDrug(((DrugTypeResponse) materialSpinner.getSelectedItem()).getId(),drug_add_name);
//
//
//                    }
//
//                    @Override
//                    public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
//
//                    }
//                });
//
//                drug_add_repeat.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showDateTime(PatientDrugSummary.this, drug_add_repeat);
//                    }
//                });
//
//                drug_add_time.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showDateTime_time(PatientDrugSummary.this, drug_add_time);
//                    }
//                });
//
//                ArrayAdapter<DrugTypeResponse> adapter =
//                        new ArrayAdapter<DrugTypeResponse>(PatientDrugSummary.this, R.layout.spinner_view
//                                , drugTypeResponses);
//                adapter.setDropDownViewResource(R.layout.spinner_view);
//                drug_add_type.setAdapter(adapter);
//
//                alertDialog.setTitle("Add");
//
//                alertDialog.setIcon(R.mipmap.ic_launcher);
//
//
//
//                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        if (mAwesomeValidation.validate()) {
//                            addAllergy("2019-08-01",
//                                    ((DrugTypeResponse) drug_add_type.getSelectedItem()).getId(),
//                                    drug_add_duration.getText().toString(),
//                                    drug_add_repeat.getText().toString(),
//                                    drug_add_time.getText().toString(),
//                                    drug_add_take.getText().toString());
//
//                        }
//
//
//                    }
//                });
//                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancle", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//
//                alertDialog.show();

                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return super.onCreateOptionsMenu(menu);
    }


    private void setToolbar() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Vital Summary"
        );
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.md_white_1000));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable drawable = toolbar.getNavigationIcon();
        drawable.setColorFilter(ContextCompat.getColor(this, R.color.md_white_1000), PorterDuff.Mode.SRC_ATOP);


    }
}
