package com.demo.doctor.View.Doctor.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demo.doctor.Model.PatientResponse;
import com.demo.doctor.R;
import com.demo.doctor.Utils.DoctorConstants;
import com.demo.doctor.View.Doctor.PatientAddEditActivity;

public class DoctorPatientDeatilsFragment extends Fragment {
    private PatientResponse patientResponse;

    TextView patient_name, patient_mobile, patient_email, patient_dob, patient_weight, patient_height;

    public DoctorPatientDeatilsFragment(PatientResponse patientResponse) {
        this.patientResponse = patientResponse;


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_patient_details, container, false);
        view.findViewById(R.id.patientEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callEdit();
            }
        });

        initView(view);
        initData();

        return view;
    }

    void initView(View view) {
        patient_name = view.findViewById(R.id.patient_name);
        patient_mobile = view.findViewById(R.id.patient_mobile);
        patient_email = view.findViewById(R.id.patient_email);
        patient_dob = view.findViewById(R.id.patient_dob);
        patient_weight = view.findViewById(R.id.patient_weight);
        patient_height = view.findViewById(R.id.patient_height);

    }

    void initData() {
        patient_name.setText(patientResponse.getFirstName() + "" + patientResponse.getMiddleName() + "" + patientResponse.getLastName());

        patient_mobile.setText(patientResponse.getMobileNumber());
        patient_email.setText(patientResponse.getEmail());
        patient_dob.setText(patientResponse.getDateOfBirth());
        patient_weight.setText("0");
        patient_height.setText("0");
    }

    void callEdit() {

        Intent intent = new Intent(getActivity(), PatientAddEditActivity.class);
        intent.putExtra(DoctorConstants.Intent.PATIENT_DETAILS,patientResponse);
        intent.putExtra(DoctorConstants.Intent.PATIENT_INPUT_TYPE,"edit");
        startActivity(intent);

    }
}