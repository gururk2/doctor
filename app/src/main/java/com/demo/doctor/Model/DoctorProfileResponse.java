package com.demo.doctor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorProfileResponse {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("doctor_name")
        @Expose
        private String doctorName;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("middle_name")
        @Expose
        private String middleName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("specialty")
        @Expose
        private String specialty;
        @SerializedName("sub_specialty")
        @Expose
        private String subSpecialty;
        @SerializedName("telephone")
        @Expose
        private String telephone;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("date_of_birth")
        @Expose
        private String dateOfBirth;
        @SerializedName("cell_number")
        @Expose
        private String cellNumber;
        @SerializedName("alter_number")
        @Expose
        private String alterNumber;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("practice_contact")
        @Expose
        private String practiceContact;
        @SerializedName("clinic_address")
        @Expose
        private String clinicAddress;
        @SerializedName("experiance")
        @Expose
        private String experiance;
        @SerializedName("qualification")
        @Expose
        private String qualification;
        @SerializedName("institute")
        @Expose
        private String institute;
        @SerializedName("fellowship")
        @Expose
        private String fellowship;
        @SerializedName("ima_no")
        @Expose
        private String imaNo;
        @SerializedName("licence")
        @Expose
        private String licence;
        @SerializedName("receive_otp")
        @Expose
        private String receiveOtp;
        @SerializedName("photo")
        @Expose
        private String photo;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("timeslot")
        @Expose
        private String timeslot;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getSpecialty() {
            return specialty;
        }

        public void setSpecialty(String specialty) {
            this.specialty = specialty;
        }

        public String getSubSpecialty() {
            return subSpecialty;
        }

        public void setSubSpecialty(String subSpecialty) {
            this.subSpecialty = subSpecialty;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getCellNumber() {
            return cellNumber;
        }

        public void setCellNumber(String cellNumber) {
            this.cellNumber = cellNumber;
        }

        public String getAlterNumber() {
            return alterNumber;
        }

        public void setAlterNumber(String alterNumber) {
            this.alterNumber = alterNumber;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPracticeContact() {
            return practiceContact;
        }

        public void setPracticeContact(String practiceContact) {
            this.practiceContact = practiceContact;
        }

        public String getClinicAddress() {
            return clinicAddress;
        }

        public void setClinicAddress(String clinicAddress) {
            this.clinicAddress = clinicAddress;
        }

        public String getExperiance() {
            return experiance;
        }

        public void setExperiance(String experiance) {
            this.experiance = experiance;
        }

        public String getQualification() {
            return qualification;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public String getInstitute() {
            return institute;
        }

        public void setInstitute(String institute) {
            this.institute = institute;
        }

        public String getFellowship() {
            return fellowship;
        }

        public void setFellowship(String fellowship) {
            this.fellowship = fellowship;
        }

        public String getImaNo() {
            return imaNo;
        }

        public void setImaNo(String imaNo) {
            this.imaNo = imaNo;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getReceiveOtp() {
            return receiveOtp;
        }

        public void setReceiveOtp(String receiveOtp) {
            this.receiveOtp = receiveOtp;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTimeslot() {
            return timeslot;
        }

        public void setTimeslot(String timeslot) {
            this.timeslot = timeslot;
        }

    }
}
