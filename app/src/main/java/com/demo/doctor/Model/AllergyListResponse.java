package com.demo.doctor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllergyListResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("allergy_type")
    @Expose
    private String allergyType;
    @SerializedName("allergy_name")
    @Expose
    private String allergyName;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("appointment_on")
    @Expose
    private String appointmentOn;
    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("allergy_idtype")
    @Expose
    private String allergyIdtype;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAllergyType() {
        return allergyType;
    }

    public void setAllergyType(String allergyType) {
        this.allergyType = allergyType;
    }

    public String getAllergyName() {
        return allergyName;
    }

    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getAppointmentOn() {
        return appointmentOn;
    }

    public void setAppointmentOn(String appointmentOn) {
        this.appointmentOn = appointmentOn;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getAllergyIdtype() {
        return allergyIdtype;
    }

    public void setAllergyIdtype(String allergyIdtype) {
        this.allergyIdtype = allergyIdtype;
    }

}
