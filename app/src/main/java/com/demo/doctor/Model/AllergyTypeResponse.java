package com.demo.doctor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllergyTypeResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("allergy_type")
    @Expose
    private String allergyType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAllergyType() {
        return allergyType;
    }

    public void setAllergyType(String allergyType) {
        this.allergyType = allergyType;
    }

    @Override
    public String toString() {
        return allergyType;
    }

}
