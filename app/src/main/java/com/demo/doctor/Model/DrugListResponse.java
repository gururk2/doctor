package com.demo.doctor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrugListResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_drugtype")
    @Expose
    private String idDrugtype;
    @SerializedName("dosage")
    @Expose
    private String dosage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdDrugtype() {
        return idDrugtype;
    }

    public void setIdDrugtype(String idDrugtype) {
        this.idDrugtype = idDrugtype;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }



    @Override
    public String toString() {
        return dosage;
    }

}
