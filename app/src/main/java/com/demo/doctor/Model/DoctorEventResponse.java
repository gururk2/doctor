package com.demo.doctor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorEventResponse {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("event_name")
        @Expose
        private String eventName;
        @SerializedName("event_desc")
        @Expose
        private String eventDesc;
        @SerializedName("event_fromdatetime")
        @Expose
        private String eventFromdatetime;
        @SerializedName("location")
        @Expose
        private String location;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEventName() {
            return eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEventDesc() {
            return eventDesc;
        }

        public void setEventDesc(String eventDesc) {
            this.eventDesc = eventDesc;
        }

        public String getEventFromdatetime() {
            return eventFromdatetime;
        }

        public void setEventFromdatetime(String eventFromdatetime) {
            this.eventFromdatetime = eventFromdatetime;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

    }
}