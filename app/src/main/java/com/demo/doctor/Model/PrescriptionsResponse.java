package com.demo.doctor.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrescriptionsResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("drug_type")
    @Expose
    private String drugType;
    @SerializedName("drug_name")
    @Expose
    private String drugName;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("repeat_same")
    @Expose
    private String repeatSame;
    @SerializedName("time_of_the_day")
    @Expose
    private String timeOfTheDay;
    @SerializedName("to_be_taken")
    @Expose
    private String toBeTaken;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("appointment_on")
    @Expose
    private String appointmentOn;
    @SerializedName("event_id")
    @Expose
    private String eventId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDrugType() {
        return drugType;
    }

    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRepeatSame() {
        return repeatSame;
    }

    public void setRepeatSame(String repeatSame) {
        this.repeatSame = repeatSame;
    }

    public String getTimeOfTheDay() {
        return timeOfTheDay;
    }

    public void setTimeOfTheDay(String timeOfTheDay) {
        this.timeOfTheDay = timeOfTheDay;
    }

    public String getToBeTaken() {
        return toBeTaken;
    }

    public void setToBeTaken(String toBeTaken) {
        this.toBeTaken = toBeTaken;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getAppointmentOn() {
        return appointmentOn;
    }

    public void setAppointmentOn(String appointmentOn) {
        this.appointmentOn = appointmentOn;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

}
