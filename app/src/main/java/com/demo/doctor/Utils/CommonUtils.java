package com.demo.doctor.Utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

import timber.log.Timber;

public class CommonUtils {
    private static Toast mToast;
    private static final String TAG = "Utils";

    // Fast Implementation
    public static String inputStreamToString(InputStream is) throws IOException {
        String line = "";
        StringBuilder total = new StringBuilder();

        // Wrap a BufferedReader around the InputStream
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        // Read response until the end
        while ((line = rd.readLine()) != null) {
            total.append(line);
        }

        // Return full string
        rd.close();
        is.close();
        return total.toString();
    }

    public static byte[] inputStreamToBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        byteBuffer.close();
        inputStream.close();
        return byteBuffer.toByteArray();
    }

    // public static byte[] base64Encode(byte[] buf) {
    // return Base64.encode(buf, Base64.NO_WRAP | Base64.URL_SAFE);
    // }
    //
    // public static byte[] base64Decode(String buf) {
    // return ChatUtils.base64decode(buf, Base64.NO_WRAP | Base64.URL_SAFE);
    // }

    public static String makePagerFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    public static void makeToast(Context context, String toast) {
        if (context == null)
            return;

        if (mToast == null) {
            mToast = Toast.makeText(context, toast, Toast.LENGTH_SHORT);
        }

        mToast.setText(toast);
        mToast.setGravity(android.view.Gravity.CENTER, 0, 0);
        mToast.show();
    }

    public static void makeLongToast(Context context, String toast) {
        if (context == null)
            return;

        if (mToast == null) {
            mToast = Toast.makeText(context, toast, Toast.LENGTH_LONG);
        }
        mToast.setGravity(android.view.Gravity.CENTER, 0, 0);
        mToast.setText(toast);
        mToast.show();
    }

    public static String getSharedPrefsString(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(DoctorConstants.PrefNames.PREFS_FILE, android.content.Context.MODE_PRIVATE);
        return settings.getString(key, null);
    }

    public static void putSharedPrefsString(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(DoctorConstants.PrefNames.PREFS_FILE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        if (value == null) {
            editor.remove(key);
        } else {
            editor.putString(key, value);
        }
        editor.commit();

    }

    public static boolean getSharedPrefsBoolean(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(DoctorConstants.PrefNames.PREFS_FILE, android.content.Context.MODE_PRIVATE);
        return settings.getBoolean(key, false);
    }

    public static void putSharedPrefsBoolean(Context context, String key, boolean value) {
        SharedPreferences settings = context.getSharedPreferences(DoctorConstants.PrefNames.PREFS_FILE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void removePref(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(DoctorConstants.PrefNames.PREFS_FILE, android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

    public static SharedPreferences getGlobalSharedPrefs(Context context) {
        return context.getSharedPreferences(DoctorConstants.PrefNames.PREFS_FILE, android.content.Context.MODE_PRIVATE);
    }

    public static HashMap<String, Integer> jsonToMap(JSONObject jsonObject) {
        try {
            HashMap<String, Integer> outMap = new HashMap<String, Integer>();

            @SuppressWarnings("unchecked")
            Iterator<String> names = jsonObject.keys();
            while (names.hasNext()) {
                String name = names.next();
                outMap.put(name, jsonObject.getInt(name));
            }

            return outMap;

        } catch (JSONException e) {
            Timber.tag(TAG).e(e);
        }
        return null;

    }


    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static void logIntent(String tag, Intent intent) {
        if (intent != null) {
            Uri uri = intent.getData();
            String action = intent.getAction();
            String type = intent.getType();
            Bundle extras = intent.getExtras();
            Set<String> categories = intent.getCategories();

            Timber.tag(tag).v("Intent uri: %s", uri);
            Timber.tag(tag).v("Intent action: %s", action);
            Timber.tag(tag).v("Intent type: %s", type);

            Timber.tag(tag).v("Intent categories: " + (categories == null ? "null" : categories.toString()));

            if (extras != null) {
                for (String extra : extras.keySet()) {
                    Timber.tag(tag).v("Intent extra, key: %s, value: %s", extra, extras.get(extra));
                }
            }
        }
    }

    public static void clearIntent(Intent intent) {
        if (intent != null) {
            intent.setData(null);
            intent.setAction(null);
            intent.setType(null);
            if (intent.getExtras() != null) {
                for (String extra : intent.getExtras().keySet()) {
                    intent.removeExtra(extra);
                }
            }
        }
    }

    public static String getResourceString(Context context, String name) {
        int nameResourceID = context.getResources().getIdentifier(name, "string", context.getApplicationInfo().packageName);
        if (nameResourceID == 0) {
            throw new IllegalArgumentException("No resource string found with name " + name);
        } else {
            return context.getString(nameResourceID);
        }
    }

    public static ArrayList<String> getToEmails(Context context) {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        ArrayList<String> emailAddresses = new ArrayList(accounts.length);
        for (android.accounts.Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                if (!emailAddresses.contains(account.name.toLowerCase())) {
                    emailAddresses.add(account.name);
                }

            }
        }

        Collections.sort(emailAddresses);

        return emailAddresses;
    }
}
