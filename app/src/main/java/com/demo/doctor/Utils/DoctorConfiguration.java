package com.demo.doctor.Utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.demo.doctor.BuildConfig;

import java.util.Properties;

import timber.log.Timber;

public class DoctorConfiguration {
    public static final int SSL_NOT_STRICT = 0;
    private final static int IMAGE_DISPLAY_HEIGHT_MULT = 200;
    // private final static int QR_DISPLAY_SIZE = 200;

    private static final String TAG = "Configuration";
    private static Properties mConfigProperties;
    private static boolean mStrictSsl;
    private static String mBaseUrl;

    private static int mImageDisplayHeight;
    private static int mQRDisplaySize;

    private static boolean mBackgroundImageSet;

    public static void LoadConfigProperties(Context context) {
        // Read from the /res/raw directory
        try {

            mBaseUrl = BuildConfig.SERVER_URL;

            // figure out image and QR display size based on screen size
            Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);

            // if (metrics.densityDpi == DisplayMetrics.DENSITY_XXHIGH) {
            mImageDisplayHeight = (int) (metrics.density * IMAGE_DISPLAY_HEIGHT_MULT);
            mQRDisplaySize = (int) (metrics.density * IMAGE_DISPLAY_HEIGHT_MULT);
            //
            Timber.tag(TAG).v( "density: %f, densityDpi: %d, imageHeight: %d", metrics.density, metrics.densityDpi, mImageDisplayHeight);

            Timber.tag(TAG).v(  "baseUrl: %s", DoctorConfiguration.getBaseUrl());
        }
        catch (Exception e) {
            Timber.tag(TAG).e(  e, "could not load configuration properties");
        }
    }

    public static Properties GetConfigProperties() {
        return mConfigProperties;
    }

    public static boolean isSslCheckingStrict() {
        return mStrictSsl;
    }

    public static String getBaseUrl() {
        return mBaseUrl;
    }

    public static int getImageDisplayHeight() {
        return mImageDisplayHeight;

    }

    public static int getQRDisplaySize() {
        return mQRDisplaySize;
    }

    public static void setBackgroundImageSet(boolean set) {
        mBackgroundImageSet = set;
    }

    public static boolean isBackgroundImageSet() {
        return mBackgroundImageSet;
    }
}