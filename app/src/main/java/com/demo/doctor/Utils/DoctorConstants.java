package com.demo.doctor.Utils;

public class DoctorConstants {




    public static final String EXTRA_IS_ROOT_FRAGMENT = ".extra_is_root_fragment";


    public class PrefNames {
        public final static String PREFS_FILE = "doctor_preferences";

        public final static String DOCTOR_NAME = "doctor_name";
        public final static String DOCTOR_F_NAME = "first_name";
        public final static String DOCTOR_M_NAME = "middle_name";
        public final static String DOCTOR_L_NAME = "last_name";
        public final static String DOCTOR_EMAIL = "email";
        public final static String DOCTOR_PASSWORD = "password";
        public final static String DOCTOR_GENDER = "gender";
        public final static String DOCTOR_SPECIALTY = "specialty";
        public final static String DOCTOR_TELEPHONE = "telephone";
        public final static String DOCTOR_MOBILE = "mobile";
        public final static String DOCTOR_DOB = "date_of_birth";
        public final static String DOCTOR_CELL = "cell_number";
        public final static String DOCTOR_ALTER_NUMBER = "alter_number";
        public final static String DOCTOR_ADDRESS = "address";
        public final static String DOCTOR_PINCODE = "pincode";
        public final static String DOCTOR_STATE = "state";
        public final static String DOCTOR_CITY = "city";
        public final static String DOCTOR_PRACTICE_CONTACT = "practice_contact";
        public final static String DOCTOR_CLINC_ADDRESS = "clinic_address";
        public final static String DOCTOR_EXPERENCE = "experiance";
        public final static String DOCTOR_QUALIFICATION = "qualification";
        public final static String DOCTOR_INSTITUTE = "institute";
        public final static String DOCTOR_FELLOWSHIP = "fellowship";
        public final static String DOCTOR_IMA_NO = "ima_no";
        public final static String DOCTOR_LICENCE = "licence";
        public final static String DOCTOR_RECIVE_OTP = "receive_otp";
        public final static String DOCTOR_PHOTO = "photo";
        public final static String DOCTOR_OTP = "otp";
        public final static String DOCTOR_STATUS = "status";
        public final static String DOCTOR_TIME_SLOAT = "timeslot";


        public static final String DOCTOR_ID = "id";
        public static final String DOCTOR_PATIENT_REFRESH = "refersh";
    }

    public class Intent{
        public final static String PATIENT_DETAILS = "patient_details";
        public static final String PATIENT_INPUT_TYPE = "input_type";
        public static final String PATIENT_ID = "patient_id";
    }
    public class MimeTypes {
        public final static String TEXT = "text/plain";
        public final static String IMAGE = "image/";
        public final static String M4A = "audio/mp4";
        public final static String DRIVE_FOLDER = "application/vnd.google-apps.folder";
        public final static String DRIVE_FILE = "application/vnd.google-apps.file";
        public final static String SURESPOT_IDENTITY = "application/ssi";
    }

    public class IntentRequestCodes {
        public final static int NEW_MESSAGE_NOTIFICATION = 0;
        public final static int INVITE_REQUEST_NOTIFICATION = 1;
        public final static int INVITE_RESPONSE_NOTIFICATION = 2;
        public final static int FOREGROUND_NOTIFICATION = 3;
        public final static int REQUEST_EXISTING_IMAGE = 4;
        public final static int REQUEST_SELECT_IMAGE = 5;
        public final static int REQUEST_SETTINGS = 6;
        public final static int LOGIN = 7;
        public final static int REQUEST_CAPTURE_IMAGE = 8;
        public final static int PICK_CONTACT = 9;
        public final static int REQUEST_SELECT_FRIEND_IMAGE = 10;
        public final static int BACKUP_NOTIFICATION = 11;
        public final static int CHOOSE_GOOGLE_ACCOUNT = 12;
        public final static int REQUEST_GOOGLE_AUTH = 13;
        public final static int SYSTEM_NOTIFICATION = 14;
        public final static int PURCHASE = 15;
        public final static int BACKGROUND_CACHE_NOTIFICATION = 16;
    }
}
